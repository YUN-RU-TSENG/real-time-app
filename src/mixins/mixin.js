export default {
  filters: {
    $_indexDisplay(index) {
      return index + 1;
    },
    $_indexChineseDisplay(index) {
      return (index + 1).toLocaleString("zh-u-nu-hanidec");
    }
  },
  methods: {
    $_nextPage(data) {
      if (this.hasOwnProperty("cacheComponents")) {
        this.cacheComponents.push(this.componentId);
      }
      this.componentId = data;
    },
    $_setTimer(data) {
      data.componentId && this.$_nextPage(data.componentId);

      const { level, section, nextLevel, nextSection } = data.missionTimeData;
      const {
        level: missionCompleteLevel,
        section: missionCompleteSection
      } = this.$store.getters.missionCompleteLevel;

      if (missionCompleteLevel === 1 && missionCompleteSection === 2 && level === 1 && section === 2) alert(`該任務小節2-2已完成`);
      if (missionCompleteLevel === 2 && missionCompleteSection === 2 && level === 2 && section === 2) alert(`該任務小節3-2已完成`);
      if (missionCompleteLevel === 3 && missionCompleteSection === 2 && level === 3 && section === 2) alert(`該任務小節4-2已完成`);

      if (
        level === missionCompleteLevel &&
        section === missionCompleteSection
      ) {
        let levelData = {
          level: nextLevel,
          section: nextSection
        };
        this.$store.commit("setMissionCompleteLevel", levelData);
        this.$store.commit("setMissionLevelTime", levelData);
        if (data.missionTimeData.complete) {
          this.$router.push({ name: "missionComplete" });
          this.$store.commit(
            "setMissionCompleteLevelCache",
            this.$store.getters.missionCompleteLevel.level
          );
        }
      }
    }
  }
};
