export default {
  // 顯示任務完成的使用者狀態，分為深、淺兩組
  usersComplete: {
    // 淺組別是否完成，true 代表完成、false 代表未完成
    shallow: [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false
    ],
    // 深組別是否完成，true 代表完成、false 代表未完成
    deep: [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false
    ]
  },
  usersShallowData: [
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    }
  ],
  usersDeepData: [
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    },
    {
      id: null,
      gender: null,
      name: null,
      missionCompleteLevel: null,
      missionLevelTime: [
        [null, null, null, null],
        [null, null, null, null],
        [null, null, null, null, null],
        [null, null, null, null, null]
      ]
    }
  ]
};
