export default {
  usersCompleteShallow: state => state.usersComplete.shallow,
  usersCompleteDeep: state => state.usersComplete.deep
};
