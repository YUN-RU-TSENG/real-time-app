export default {
  name: state => state.name,
  id: state => state.id,
  missionDepth: state => state.missionDepth,
  pageComponent: state => state.pageComponent,
  pageHome: state => state.pageHome,
  prePage: state => state.prePage,
  changePageComponent: state => state.changePageComponent,
  missionCompleteLevel: state => state.missionCompleteLevel,
  missionCompleteLevelCache: state => state.missionCompleteLevelCache,
  jitterSeconds: state => state.jitterSeconds,
  setGenderAbbreviative: state => (state.gender === "male" ? "M" : "F"),
  missionLevelState: state =>
    state.missionLevelTime.map(value => value.every(value => value)),
  missionCurrentLevel: state => {
    return {
      level: state.missionCurrentLevel.level - 1,
      section: state.missionCurrentLevel.level - 1
    };
  },
  stopSecond: state => {
    switch (state.missionCompleteLevel.level) {
      case 0:
        return 13;
      case 1:
        return 14;
      case 2:
        return 18;
      case 3:
        return 12;
    }
  },
  userSymbol: (state, getters) => {
    let displayId = null;
    const group = 72;
    console.log(state.id, getters.id, state.missionDepth, getters.missionDepth);
    if (getters.missionDepth === "deep") displayId = getters.id + 1;
    if (getters.missionDepth === "shallow") displayId = getters.id + group + 1;
    return (getters.setGenderAbbreviative + displayId).toString();
  },
  setMissionCurrentTime: state => level => {
    // 另外設置一組物件，儲存時間。
    let timeCount = [];
    state.missionLevelTime
      .find((value, index) => index === level)
      .reduce((acc, cur, index) => {
        if (typeof cur !== "object") {
          acc = new Date(acc);
          cur = new Date(cur);
        }
        timeCount[index - 1] = (cur.getTime() - acc.getTime()) / 1000;
        return cur;
      });
    return timeCount.map(value => {
      let time = {
        second: value % 60,
        minute: (value - (value % 60)) / 60
      };
      let unitDigit = /^\b(?=\d$)/;
      time.second = time.second
        .toFixed(2)
        .toString()
        .replace(unitDigit, "0");
      time.minute = time.minute.toString().replace(unitDigit, "0");
      return time;
    });
  }
};
