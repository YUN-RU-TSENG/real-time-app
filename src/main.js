import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;


const moment = require("moment");
require("moment/locale/zh-tw");
Vue.use(require("vue-moment"), {
  moment
});

console.log(moment.locale())

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
